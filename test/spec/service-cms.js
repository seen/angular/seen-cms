  /* 
   * To change this license header, choose License Headers in Project Properties.
   * To change this template file, choose Tools | Templates
   * and open the template in the editor.
   */
  'use strict';
  angular.module('TestModule', ['seen-test', 'seen-cms']);
  describe('Factories ', function () {
      var $cms;
      var $test;
      beforeEach(function () {
          module('TestModule');
          inject(function (_$cms_, _$test_) {
              $cms = _$cms_;
              $test = _$test_;
          });
      });


      var types = [{
              name: 'Content',
              url: '/api/v2/cms/contents'
          }, {
              name: 'Term',
              url: '/api/v2/cms/terms'
          }, {
              name: 'TermTaxonomy',
              url: '/api/v2/cms/term-taxonomies'
          }];
      for (var i = 0; i < types.length; i++) {
          var type = types[i];
          // Function test
          it('should contains ' + type.name + ' functions', function () {
              expect($test).not.toBeNull();
              $test.serviceCollectionFunction($cms, type);
          });
          // Get items
          it('should call GET:' + type.url + ' to list items form collectinos', function (done) {
              $test.serviceGetList($cms, type, done);
          });
          // delete items form collection
          it('should call DELETE:' + type.url + ' to delete items form collectinos', function (done) {
              $test.serviceDeleteList($cms, type, done);
          });
          // PUT items form collection
          it('should call PUT:' + type.url + ' to add items form collectinos', function (done) {
              $test.servicePutList($cms, type, done);
          });

          // Get an item with id
          it('should call GET:' + type.url + '/1 to an item form collectinos', function (done) {
              $test.serviceGetItem($cms, type, done);
          });
          // Get an item with id
          it('should call PUT:' + type.url + ' to an item form collectinos', function (done) {
              $test.servicePutItem($cms, type, done);
          });
      }
  });
