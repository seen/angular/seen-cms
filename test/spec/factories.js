/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
angular.module('TestModule', ['seen-test', 'seen-cms']);

describe('Factories ', function () {
	var $cms;
	var $test;
	beforeEach(function () {
		module('TestModule');
		inject(function (_$cms_, _$test_) {
			$cms = _$cms_;
			$test = _$test_;
		});
	});

	function testForResource(factory, resource) {
		it('should contains function of ' + resource.name, function () {
			$test.factoryCollectionFunctions(factory, resource);
		});
	}

	function testFor(factory) {
		// Function test
		it('should contains basic functions', function () {
			$test.factoryBasicFunctions(factory);
		});

		it('should call POST:' + factory.url + ' to update', function (done) {
			$test.factoryUpdateFunctions(factory, done);
		});
		it('should call DELETE:' + factory.url + ' to update', function (done) {
			$test.factoryDeleteFunctions(factory, done);
		});

		if (angular.isArray(factory.resources)) {
			for (var j = 0; j < factory.resources.length; j++) {
				testForResource(factory, factory.resources[j]);
			}
		}
	}

//	TODO: this item should be checked in CmsContent. Now, it encounters with error in testing.
//	Then add to resources of CmsFactory in factories
//	{
//	name: 'Value',
//	type: 'binary',
//	url: '/download'
//	}, 
	var factories =
		[{
			factory: 'CmsContent',
			url: '/api/v2/cms/contents',
			resources: [{
				name: 'TermTaxonomy',
				factory: 'CmsTermTaxonomy',
				type: 'collection',
				url: '/term-taxonomies'
			},{
				name: 'Metadata',
				factory: 'CmsContentMetadata',
				type: 'collection',
				url: '/metas'
			}]
		}, {
			factory: 'CmsTermTaxonomy',
			url: '/api/v2/cms/term-taxonomies',
			resources: [{
				name: 'Content',
				factory: 'CmsContent',
				type: 'collection',
				url: '/contents'
			}]
		}, {
			factory: 'CmsTerm',
			url: '/api/v2/cms/terms',
			resources: [{
				name: 'TermTaxonomy',
				factory: 'CmsTermTaxonomy',
				type: 'collection',
				url: '/term-taxonomies'
			}]
		}];

	for (var i = 0; i < factories.length; i++) {
		testFor(factories[i]);
	}
});

