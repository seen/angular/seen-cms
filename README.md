# Seen CMS

master:
[![pipeline status](https://gitlab.com/seen/angular/seen-cms/badges/master/pipeline.svg)](https://gitlab.com/seen/angular/seen-cms/commits/master) 
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/e4e86a8bd73e4b059c1448f7ce779bd9)](https://www.codacy.com/app/seen/seen-cms?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=seen/angular/seen-cms&amp;utm_campaign=Badge_Grade)

develop:
[![pipeline status - develop](https://gitlab.com/seen/angular/seen-cms/badges/develop/pipeline.svg)](https://gitlab.com/seen/angular/seen-cms/commits/develop)